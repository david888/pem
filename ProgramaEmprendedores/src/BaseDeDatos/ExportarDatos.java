/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDeDatos;

import java.io.File;
import javax.swing.JTable;

/**
 *
 * @author david
 */
public interface ExportarDatos {
   
    public void generarExcel(JTable tabla, File destino);
    
    
}
