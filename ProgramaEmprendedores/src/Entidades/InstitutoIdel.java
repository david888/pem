/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class InstitutoIdel {
    private int id;
    private String nombre;
    private String correo;
    private String descripcion;
    private int telefono;
    private String ubicacion;
    private List<Empleado> administradores;
    private List<Programa> programas;

    //private List<Programa> programas;

    /*
    public static void main(String [] args){
        System.out.println("hello word!");
        
        int id = 1, telefono = 111111;
        String nombre = "IDEL", correo = "xxx@gmail.com", descripcion = "Instituto de desarrollo local de florencio varela", ubicacion = "florencio varela";
        InstitutoIDEL idel = new InstitutoIDEL(id, nombre, correo, descripcion, telefono, ubicacion);
        
        System.out.println(idel.toString());
        
        String nombreAd = "silvia", apellido = "Oro", username = "silvia", password = "1235", correoAd = "asilvia.oro@gmail.com";
        int dni = 1;
        Date fechaNac = new Date();
        Empleado ad = new Empleado(username, password, nombreAd, apellido, dni, fechaNac);
        
        System.out.println(ad.toString());
    }
     */
    
    
    public InstitutoIdel(int id, String nombre, String correo, String descripcion, int telefono, String ubicacion) {
        this.id = id;
        this.nombre = nombre;
        this.correo = correo;
        this.descripcion = descripcion;
        this.telefono = telefono;
        this.ubicacion = ubicacion;
        administradores = new ArrayList<>();
        programas = new ArrayList<>();

    }

  

   

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public List<Empleado> getAdministradores() {
        return administradores;
    }

    public void setAdministradores(List<Empleado> administradores) {
        this.administradores = administradores;
    }

    
    //public void agregarPrograma()
    


    public void setProgramas(List<Programa> programas) {
        this.programas = programas;
    }

    public List<Programa> getProgramas() {
        return programas;
    }


    @Override
    public String toString() {
        return "id:" + id + " nombre:" + nombre + " descripcion:" + descripcion;
    }
    public void agregarAdministrativo(Empleado admistrativo){
        administradores.add(admistrativo);
    }
    public void agregarPrograma(Programa pograma) {
        programas.add(pograma);
    }
}
