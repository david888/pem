/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package VentanaDialogo;

import BaseDeDatos.InterfazGeneral;
import Entidades.Barrio;
import Entidades.Emprededor;
import Entidades.Localidad;
import Entidades.Rubro;
import Aplicacion.ConvertidorDeFechas;
import Ventanas.FramePrincipal;
import Ventanas.PanelDeFondo;
import Ventanas.PanelHorizontal;
import java.time.LocalDate;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author david
 */
public class EditarModalEmprendedor extends Dialogo {

    private Emprededor e;

    /**
     * Creates new form CargaDialog
     */
    public EditarModalEmprendedor(FramePrincipal parent, String titulo) {
        super(parent, titulo, true);
        initComponents();
        
        this.jPanel4.add( new PanelHorizontal());
        jPanel4.repaint();

        ConvertidorDeFechas.generarAños(comboañoEmprededor);

        this.buttonGroup1.add(this.opcionFemeninoEmprededor);
        this.buttonGroup1.add(this.opcionMasculinoEmprededor);
       
        

        List<Localidad> domicilios = parent.getLocalidades();

        for (Localidad domicilio : domicilios) {
            comboLocalidadEmprededor.addItem(domicilio.getNombre());
        }

        List<Rubro> rubros = parent.getRubros();
        for (Rubro rubro : rubros) {
            comboRubroEmprededor.addItem(rubro.getNombre());
        }

    }

    public void cargaModal() {
        Emprededor emprededor = null;

        JTable tablaEmprendedor = getPadre().getTablaEmprendedor();

        if (tablaEmprendedor.getSelectedRow() != -1) {

            int fila = tablaEmprendedor.getSelectedRow();
            int columna = 3;

            int dni = Integer.parseInt(tablaEmprendedor.getValueAt(fila, columna).toString());

            e = emprededor = getPadre().getEmprendedor(dni);

        }

        this.textnombreEmpredor.setText(emprededor.getNombre());
        this.textapellidoEmorededor.setText(emprededor.getApellido());
        this.textdniEmprededor.setText(String.valueOf(emprededor.getDni()));
        this.textcorreoEmprededor.setText(emprededor.getCorreo());
        this.textttelefonoEmprededor.setText(String.valueOf(emprededor.getTelefono()));

        if (emprededor.getSexo().equals("masculino")) {
            this.opcionMasculinoEmprededor.setSelected(true);
        } else {
            this.opcionFemeninoEmprededor.setSelected(true);
        }

        LocalDate fecha = emprededor.getFechaNacimiento();

        //año, mes, dia
        ConvertidorDeFechas.fechaAComboBox(comboañoEmprededor, combomesEmprededor, combodiaEmprededor, fecha);

        System.out.println(emprededor.toString());

        int i = 0, cantidadDeElementos = comboLocalidadEmprededor.getComponentCount();

        for (; i < cantidadDeElementos; i++) {
            //System.out.println(comboLocalidadEmprededor.getItemAt(i));

            if (comboLocalidadEmprededor.getItemAt(i).equals(emprededor.getLocalidad())) {
                System.out.println("indice de localiad:" + i);
                comboLocalidadEmprededor.setSelectedIndex(i);
                break;
            }
        }

        this.comboBarrioEmprededor.removeAllItems();

        Localidad localidad = new Localidad(this.comboLocalidadEmprededor.getSelectedItem().toString());
        localidad.setId(this.comboLocalidadEmprededor.getSelectedIndex() + 1);

        List<Barrio> barrios = this.getPadre().getBarrios(localidad);

        int cont = 0;
        for (Barrio barrio : barrios) {
            this.comboBarrioEmprededor.addItem(barrio.getNombre());
            if (barrio.getNombre().equals(emprededor.getBarrio())) {
                comboBarrioEmprededor.setSelectedIndex(cont);
            }
            ++cont;
        }

        this.comboRubroEmprededor.removeAllItems();
        List<Rubro> rubros = getPadre().getRubros();

        cont = 0;
        for (Rubro r : rubros) {
            this.comboRubroEmprededor.addItem(r.getNombre());
            if (r.getNombre().equals(emprededor.getRubro())) {
                comboRubroEmprededor.setSelectedIndex(cont);
            }
            ++cont;
        }

        this.setVisible(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel4 = new javax.swing.JPanel();
        botonRegistrarEmprendedor = new javax.swing.JButton();
        botonCancelar = new javax.swing.JButton();
        nombre = new javax.swing.JLabel();
        apellido = new javax.swing.JLabel();
        textnombreEmpredor = new javax.swing.JTextField();
        textapellidoEmorededor = new javax.swing.JTextField();
        DNI = new javax.swing.JLabel();
        textdniEmprededor = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        textttelefonoEmprededor = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        textcorreoEmprededor = new javax.swing.JTextField();
        fecha_nacimiento = new javax.swing.JLabel();
        combodiaEmprededor = new javax.swing.JComboBox<>();
        combomesEmprededor = new javax.swing.JComboBox<>();
        comboañoEmprededor = new javax.swing.JComboBox<>();
        genero = new javax.swing.JLabel();
        opcionMasculinoEmprededor = new javax.swing.JRadioButton();
        opcionFemeninoEmprededor = new javax.swing.JRadioButton();
        jLabel6 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        comboBarrioEmprededor = new javax.swing.JComboBox<>();
        comboLocalidadEmprededor = new javax.swing.JComboBox<>();
        jLabel16 = new javax.swing.JLabel();
        comboRubroEmprededor = new javax.swing.JComboBox<>();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        botonRegistrarEmprendedor.setText("Registrar");
        botonRegistrarEmprendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRegistrarEmprendedorActionPerformed(evt);
            }
        });

        botonCancelar.setText("Cancelar");
        botonCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(botonRegistrarEmprendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(17, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonRegistrarEmprendedor)
                    .addComponent(botonCancelar))
                .addGap(22, 22, 22))
        );

        nombre.setText("Nombre");

        apellido.setText("Apellido");

        DNI.setText("DNI");

        jLabel18.setText("Telefono");

        jLabel17.setText("Correo");

        fecha_nacimiento.setText("Fecha de nacimiento");

        combodiaEmprededor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        combomesEmprededor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "enero", "febrero", "marzo", "abril", "mayo", "junio", "julio", "agosto", "septiembre", "octubre", "noviembre", "diciembre" }));

        genero.setText("Genero");

        opcionMasculinoEmprededor.setText("Masculino");

        opcionFemeninoEmprededor.setText("Femenino");

        jLabel6.setText("Localidad");

        jLabel12.setText("Barrio");

        comboLocalidadEmprededor.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboLocalidadEmprededorItemStateChanged(evt);
            }
        });

        jLabel16.setText("Rubro");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(fecha_nacimiento, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(genero)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(combodiaEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(combomesEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(comboañoEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))))
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textnombreEmpredor, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textdniEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textapellidoEmorededor, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(textttelefonoEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(DNI, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(nombre, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(114, 114, 114)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(opcionMasculinoEmprededor)
                            .addComponent(jLabel6)
                            .addComponent(comboLocalidadEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel12)
                            .addComponent(opcionFemeninoEmprededor)
                            .addComponent(comboBarrioEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(textcorreoEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 289, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel16)
                    .addComponent(comboRubroEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(51, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nombre)
                    .addComponent(apellido, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textnombreEmpredor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textapellidoEmorededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(DNI, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textdniEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textttelefonoEmprededor))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(textcorreoEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(fecha_nacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(combodiaEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(combomesEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboañoEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(genero)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(opcionMasculinoEmprededor)
                    .addComponent(opcionFemeninoEmprededor))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboLocalidadEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBarrioEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboRubroEmprededor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void comboLocalidadEmprededorItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboLocalidadEmprededorItemStateChanged

        this.comboBarrioEmprededor.removeAllItems();

        Localidad localidad = new Localidad(this.comboLocalidadEmprededor.getSelectedItem().toString());
        localidad.setId(this.comboLocalidadEmprededor.getSelectedIndex() + 1);

        List<Barrio> barrios = getPadre().getBarrios(localidad);

        for (Barrio barrio : barrios) {
            this.comboBarrioEmprededor.addItem(barrio.getNombre());
        }
    }//GEN-LAST:event_comboLocalidadEmprededorItemStateChanged

    private void botonRegistrarEmprendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRegistrarEmprendedorActionPerformed
        if (textnombreEmpredor.getText().isEmpty() || textapellidoEmorededor.getText().isEmpty() || textdniEmprededor.getText().isEmpty()
                || textttelefonoEmprededor.getText().isEmpty() || textcorreoEmprededor.getText().isEmpty()) {
            JOptionPane.showMessageDialog(null, "NO puede dejar campos vacios");
        } else {
            String nombre = textnombreEmpredor.getText();
            String apellido = textapellidoEmorededor.getText();
            int dni = Integer.parseInt(textdniEmprededor.getText());

            LocalDate fechaNac = ConvertidorDeFechas.comboBoxAFecha(comboañoEmprededor, combomesEmprededor, combodiaEmprededor);

            int telefono = Integer.parseInt(this.textttelefonoEmprededor.getText());

            //int celular = Integer.parseInt(this.jTextCelular.getText());
            Emprededor emprededor;

            //publico.setCelular(celular);
            String genero;
            if (this.opcionMasculinoEmprededor.isSelected()) {
                genero = "masculino";
            } else {
                genero = "femenino";
            }

            String barrio = String.valueOf(this.comboBarrioEmprededor.getSelectedIndex() + 1);
            String correo = textcorreoEmprededor.getText();
            String rubro = String.valueOf(comboRubroEmprededor.getSelectedIndex() + 1);
            emprededor = new Emprededor(nombre, apellido, dni, genero, fechaNac, " ", barrio, telefono, 0);
            emprededor.setRubro(rubro);
            emprededor.setCorreo(correo);

            emprededor.setId(e.getId());
            emprededor.setCodigoPublico(e.getCodigoPublico());
            emprededor.setIdEmprendedor(e.getIdEmprendedor());

            getPadre().updateEmprendedor(emprededor);

            textnombreEmpredor.setText("");
            textapellidoEmorededor.setText("");
            textdniEmprededor.setText("");
            textttelefonoEmprededor.setText("");

            dispose();
        }
    }//GEN-LAST:event_botonRegistrarEmprendedorActionPerformed

    private void botonCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCancelarActionPerformed
        dispose();
    }//GEN-LAST:event_botonCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(EditarModalEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(EditarModalEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(EditarModalEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(EditarModalEmprendedor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>


        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                EditarModalEmprendedor dialog = new EditarModalEmprendedor(new FramePrincipal(), "Registrar");
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel DNI;
    private javax.swing.JLabel apellido;
    private javax.swing.JButton botonCancelar;
    private javax.swing.JButton botonRegistrarEmprendedor;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> comboBarrioEmprededor;
    private javax.swing.JComboBox<String> comboLocalidadEmprededor;
    private javax.swing.JComboBox<String> comboRubroEmprededor;
    private javax.swing.JComboBox<String> comboañoEmprededor;
    private javax.swing.JComboBox<String> combodiaEmprededor;
    private javax.swing.JComboBox<String> combomesEmprededor;
    private javax.swing.JLabel fecha_nacimiento;
    private javax.swing.JLabel genero;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JLabel nombre;
    private javax.swing.JRadioButton opcionFemeninoEmprededor;
    private javax.swing.JRadioButton opcionMasculinoEmprededor;
    private javax.swing.JTextField textapellidoEmorededor;
    private javax.swing.JTextField textcorreoEmprededor;
    private javax.swing.JTextField textdniEmprededor;
    private javax.swing.JTextField textnombreEmpredor;
    private javax.swing.JTextField textttelefonoEmprededor;
    // End of variables declaration//GEN-END:variables
}
