/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.time.LocalDate;
import java.util.Date;

/**
 *
 * @author USER
 */
public class Emprededor extends PublicoGeneral{
    private int idEmprendedor;

    private String correo;
    private String rubro;

    public Emprededor(String nombre, String apellido, int dni, String sexo, LocalDate fechaNacimiento, String municipio, String barrio, int telefono, int celular) {
        super(nombre, apellido, dni, sexo, fechaNacimiento,municipio, barrio, telefono, celular);
    }

    public Emprededor() {
      
    }

   
    
    
    

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public void setRubro(String rubro) {
        this.rubro = rubro;
    }

    public String getCorreo() {
        return correo;
    }

    public String getRubro() {
        return rubro;
    }

   
    public int getIdEmprendedor() {
        return idEmprendedor;
    }

    public void setIdEmprendedor(int idEmprendedor) {
        this.idEmprendedor = idEmprendedor;
    }
    
}
