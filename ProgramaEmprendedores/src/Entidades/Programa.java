/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class Programa {
    private String nombre;
    private String descripcion;
    private List<Empleado> administradores;

    public Programa(String nombre, String descripcion) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.administradores = new ArrayList<>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public List<Empleado> getAdministradores() {
        return administradores;
    }

    public void setAdministradores(List<Empleado> administradores) {
        this.administradores = administradores;
    }
    
     @Override
    public String toString(){
        return "nombre:" + nombre + " descripcion:" + descripcion;
    }
    
    
}
