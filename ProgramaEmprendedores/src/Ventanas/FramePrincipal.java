/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Ventanas;

import BaseDeDatos.ExportarDatoImpl;
import BaseDeDatos.ExportarDatos;
import VentanaDialogo.CargaModalConsulta;
import BaseDeDatos.InterfazGeneral;
import BaseDeDatos.InterfazGeneralImpl;
import Entidades.Barrio;
import Entidades.Consulta;
import Entidades.Emprededor;
import Entidades.Localidad;

import Entidades.PublicoGeneral;
import Entidades.Rubro;
import Entidades.tipoConsulta;
import VentanaDialogo.CargaModalEmprendedor;
import VentanaDialogo.CargaModalPublico;
import VentanaDialogo.EditarModalConsulta;
import VentanaDialogo.EditarModalEmprendedor;
import VentanaDialogo.EditarModalPublico;
import java.io.File;
import java.time.LocalDate;
import java.util.List;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author David
 */
public class FramePrincipal extends javax.swing.JFrame {

    private InterfazGeneral repositorio;
    private ExportarDatos exportarDatos = new ExportarDatoImpl();
    private CargaModalConsulta cargaConsulta;
    private EditarModalConsulta editarConsulta;
    private CargaModalPublico cargaPublico;
    private EditarModalPublico editarPublico;
    private CargaModalEmprendedor cargarEmprendedor;
    private EditarModalEmprendedor editarEmprendedor;

    /**
     * Creates new form PanelPrincipal
     */
    public FramePrincipal() {
        repositorio = new InterfazGeneralImpl();

        initComponents();

        
        setResizable(false);
        cargarTablaPublico();
        cargarTablaEmprededor();
        cargarTablaConsulta();

        cargaConsulta = new CargaModalConsulta(this, "Registrar consulta");
        editarConsulta = new EditarModalConsulta(this, "Editar consulta");
        cargaPublico = new CargaModalPublico(this, "Registro de publico general");
        editarPublico = new EditarModalPublico(this, "Editar publico");
        cargarEmprendedor = new CargaModalEmprendedor(this, "Registrar emprendedor");
        editarEmprendedor = new EditarModalEmprendedor(this, "Editar emprendedor");

        /* this.PanelGestionEmprendedores.add(new PanelDeFondo());
        PanelGestionEmprendedores.repaint();
        this.panelConsultas.add(new PanelDeFondo());
        this.panelConsultas.repaint();
        //this.panelPublicoGeneral.add(new PanelDeFondo());
        //panelPublicoGeneral.repaint();*/
        panelPrincipal.add(new PanelDeFondo());
        panelPrincipal.repaint();

        PanelHorizontal horizontal = new PanelHorizontal();

        panelLateral1.add(horizontal);
        panelLateral1.repaint();
        panelLateral2.add( new PanelHorizontal());
        panelLateral2.repaint();
        panelLateral3.add( new PanelHorizontal());
        panelLateral3.repaint();
    }

    public void sendPublicoGeneral(PublicoGeneral publico) {
        repositorio.setPublicoGeneral(publico);

        cargarTablaPublico();
    }

    public JTable getTablaConsultas() {
        return this.tablaConsulta;
    }

    public void sendEmprendedor(Emprededor emprendedor) {
        repositorio.setEmprededor(emprendedor);

        cargarTablaEmprededor();
    }

    public void updatePublico(PublicoGeneral publico) {
        repositorio.updatePublicoGeneral(publico);

        cargarTablaPublico();
    }

    public void updateEmprendedor(Emprededor emprededor) {
        repositorio.updateEmprendedor(emprededor);

        cargarTablaEmprededor();
    }

    public void updateConsulta(Consulta consulta) {
        repositorio.updateConsulta(consulta);
        //editarConsulta.setVisible(false);
        cargarTablaConsulta();
    }

    public List<Barrio> getBarrios(Localidad localidad) {
        return repositorio.getBarrios(localidad);
    }

    public List<tipoConsulta> getTiposDeConsultas() {
        return repositorio.gettipoConsultas();
    }

    public JTable getTablaPublicoGeneral() {
        return tablaPublicoGeneral;
    }

    public void sendConsulta(Consulta consulta, PublicoGeneral ciudadano) {
        repositorio.setConsulta(consulta, ciudadano);
        cargarTablaConsulta();

    }

    public Emprededor getEmprendedor(int dni) {
        return repositorio.getEmprendedores(dni).get(0);
    }

    public PublicoGeneral getPublicoGeneral(int dni) {
        return repositorio.consultaPublicoGeneral(dni).get(0);
    }

    public List<Rubro> getRubros() {
        return repositorio.getRubros();
    }

    //funcion que carga al publico
    public void cargarTablaPublico() {

        List<PublicoGeneral> publico = repositorio.getPublicoGeneral();

        cargarTablaPublicoGeneral(publico);

    }
    //funcion que carga los emprededores

    public void cargarTablaEmprededor() {

        List<Emprededor> emprededores = repositorio.getEmprededores();

        cargarTablaEmprendedor(emprededores);
    }

    public void cargarTablaConsulta() {

        List<Consulta> consultas = repositorio.getConsultas();

        cargarTablaConsulta(consultas);
    }

    public void vaciarTabla(JTable tabla) {
        DefaultTableModel tableModel = (DefaultTableModel) tabla.getModel();
        int a = tabla.getRowCount() - 1;
        for (int i = a; i >= 0; i--) {
            tableModel.removeRow(tableModel.getRowCount() - 1);
        }

    }

    public JTable getTablaEmprendedor() {
        return this.tablaEmprededor;
    }

    public List<Localidad> getLocalidades() {
        return repositorio.getMunicipios();
    }

    public void cargarTablaEmprendedor(List<Emprededor> emprendedores) {
        LocalDate fechaActual = LocalDate.now();

        DefaultTableModel tb = (DefaultTableModel) tablaEmprededor.getModel();

        vaciarTabla(tablaEmprededor);

        int fila = 0, column = 0;
        for (Emprededor e : emprendedores) {
            tb.addRow(new Object[]{"", "", "", "", "", "", "", "", ""});
            tablaEmprededor.setValueAt(e.getIdEmprendedor(), fila, column++);
            tablaEmprededor.setValueAt(e.getNombre(), fila, column++);
            tablaEmprededor.setValueAt(e.getApellido(), fila, column++);
            tablaEmprededor.setValueAt(e.getDni(), fila, column++);
            tablaEmprededor.setValueAt(e.getSexo(), fila, column++);
            tablaEmprededor.setValueAt(e.getTelefono(), fila, column++);
            tablaEmprededor.setValueAt(e.getCelular(), fila, column++);
            tablaEmprededor.setValueAt(fechaActual.getYear() - e.getFechaNacimiento().getYear(), fila, column++);
            tablaEmprededor.setValueAt(e.getLocalidad(), fila, column++);
            tablaEmprededor.setValueAt(e.getBarrio(), fila, column++);
            tablaEmprededor.setValueAt(e.getCorreo(), fila, column++);
            tablaEmprededor.setValueAt(e.getRubro(), fila, column++);
            column = 0;
            ++fila;
        }

    }

    public void cargarTablaConsulta(List<Consulta> consultas) {
        DefaultTableModel tb = (DefaultTableModel) tablaConsulta.getModel();

        vaciarTabla(tablaConsulta);

        LocalDate fechaActual = LocalDate.now();

        //0-numero 1-apellido 2-nombre 3-dni 4-tipo 5-descripcion 
        int fila = 0;
        for (Consulta e : consultas) {
            tb.addRow(new Object[]{"", "", "", "", "", "", "", "", ""});
            tablaConsulta.setValueAt(e.getNumero(), fila, 0);
            tablaConsulta.setValueAt(e.getFecha(), fila, 1);
            tablaConsulta.setValueAt(e.getApellido(), fila, 2);
            tablaConsulta.setValueAt(e.getNombre(), fila, 3);
            tablaConsulta.setValueAt(e.getDni(), fila, 4);
            tablaConsulta.setValueAt(e.getTipo(), fila, 5);
            tablaConsulta.setValueAt(e.getDescripcion(), fila, 6);

            ++fila;
        }

    }

    public void cargarTablaPublicoGeneral(List<PublicoGeneral> publico) {
        DefaultTableModel tb = (DefaultTableModel) tablaPublicoGeneral.getModel();

        vaciarTabla(tablaPublicoGeneral);

        LocalDate fechaActual = LocalDate.now();

        //0-nombre 1-apellido 2-dni 3-sexo 4-telefono 5-celular 6-edad 7-localidad 8-barrio 9-calle
        int fila = 0, column = 0;
        for (PublicoGeneral persona : publico) {
            tb.addRow(new Object[]{"", "", "", "", "", "", "", "", ""});
            tablaPublicoGeneral.setValueAt(persona.getCodigoPublico(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getNombre(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getApellido(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getDni(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getSexo(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getTelefono(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getCelular(), fila, column++);
            tablaPublicoGeneral.setValueAt(fechaActual.getYear() - persona.getFechaNacimiento().getYear(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getLocalidad(), fila, column++);
            tablaPublicoGeneral.setValueAt(persona.getBarrio(), fila, column++);
            column = 0;
            ++fila;
        }

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        panelPrincipal = new javax.swing.JPanel();
        Estadisticas = new javax.swing.JTabbedPane();
        PanelGestionEmprendedores = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaEmprededor = new javax.swing.JTable();
        botonRegistrarEmprendedor = new javax.swing.JButton();
        botonBajaEmprendedor = new javax.swing.JButton();
        botonEditarEmprendedor = new javax.swing.JButton();
        jButton8 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        panelLateral1 = new javax.swing.JPanel();
        botonBuscarEmprendedor = new javax.swing.JButton();
        entradaBuscarEmprendedor = new javax.swing.JTextField();
        jButton2 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        panelPublicoGeneral = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaPublicoGeneral = new javax.swing.JTable();
        botonAgregarUsuario = new javax.swing.JButton();
        botonEditarPublico = new javax.swing.JButton();
        botonBajaPublico = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        panelLateral2 = new javax.swing.JPanel();
        verTodos = new javax.swing.JButton();
        entradaDeBusqueda = new javax.swing.JTextField();
        botonBuscar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        panelConsultas = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaConsulta = new javax.swing.JTable();
        botonEditarConsulta = new javax.swing.JButton();
        bajaConsulta = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        panelLateral3 = new javax.swing.JPanel();
        botonCargarConsultas = new javax.swing.JButton();
        entradaBuscarConsulta = new javax.swing.JTextField();
        botonBuscarConsulta = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        Estadisticas.setPreferredSize(new java.awt.Dimension(1200, 600));
        Estadisticas.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                EstadisticasMouseClicked(evt);
            }
        });

        tablaEmprededor.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "Apellido", "DNI", "Sexo", "Telefono", "Celular", "Edad", "localidad", "barrio", "Correo", "Rubro"
            }
        ));
        jScrollPane5.setViewportView(tablaEmprededor);

        botonRegistrarEmprendedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/añadir con color negro.png"))); // NOI18N
        botonRegistrarEmprendedor.setText("Registrar");
        botonRegistrarEmprendedor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonRegistrarEmprendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonRegistrarEmprendedorActionPerformed(evt);
            }
        });

        botonBajaEmprendedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/eliminar en color negro (2).png"))); // NOI18N
        botonBajaEmprendedor.setText("Baja");
        botonBajaEmprendedor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonBajaEmprendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBajaEmprendedorActionPerformed(evt);
            }
        });

        botonEditarEmprendedor.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/eliminar en color negro.png"))); // NOI18N
        botonEditarEmprendedor.setText("Editar");
        botonEditarEmprendedor.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonEditarEmprendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarEmprendedorActionPerformed(evt);
            }
        });

        jButton8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/icons8-editar-propiedad-24.png"))); // NOI18N
        jButton8.setText("Registrar consulta");
        jButton8.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/icons8-ms-excel-24.png"))); // NOI18N
        jButton3.setText("Generar Excel");
        jButton3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        botonBuscarEmprendedor.setText("Buscar");
        botonBuscarEmprendedor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarEmprendedorActionPerformed(evt);
            }
        });

        jButton2.setText("Ver Todos");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        jLabel2.setText("Emprendedores");

        javax.swing.GroupLayout panelLateral1Layout = new javax.swing.GroupLayout(panelLateral1);
        panelLateral1.setLayout(panelLateral1Layout);
        panelLateral1Layout.setHorizontalGroup(
            panelLateral1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateral1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addGap(117, 117, 117)
                .addComponent(jButton2)
                .addGap(18, 18, 18)
                .addComponent(entradaBuscarEmprendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscarEmprendedor)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelLateral1Layout.setVerticalGroup(
            panelLateral1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateral1Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(panelLateral1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton2)
                    .addComponent(entradaBuscarEmprendedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscarEmprendedor)
                    .addComponent(jLabel2))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout PanelGestionEmprendedoresLayout = new javax.swing.GroupLayout(PanelGestionEmprendedores);
        PanelGestionEmprendedores.setLayout(PanelGestionEmprendedoresLayout);
        PanelGestionEmprendedoresLayout.setHorizontalGroup(
            PanelGestionEmprendedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelGestionEmprendedoresLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(PanelGestionEmprendedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jButton3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButton8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonBajaEmprendedor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonRegistrarEmprendedor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonEditarEmprendedor, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 90, Short.MAX_VALUE)
                .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 948, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(40, 40, 40))
            .addComponent(panelLateral1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        PanelGestionEmprendedoresLayout.setVerticalGroup(
            PanelGestionEmprendedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PanelGestionEmprendedoresLayout.createSequentialGroup()
                .addComponent(panelLateral1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(PanelGestionEmprendedoresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(PanelGestionEmprendedoresLayout.createSequentialGroup()
                        .addComponent(botonRegistrarEmprendedor)
                        .addGap(18, 18, 18)
                        .addComponent(botonEditarEmprendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(botonBajaEmprendedor, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton8)
                        .addGap(18, 18, 18)
                        .addComponent(jButton3))
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 439, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(85, Short.MAX_VALUE))
        );

        Estadisticas.addTab("Gestion de emprededores", PanelGestionEmprendedores);

        tablaPublicoGeneral.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Codigo", "Nombre", "Apellido", "DNI", "Sexo", "Telefono", "Celular", "Edad", "Localidad", "Barrio"
            }
        ));
        jScrollPane2.setViewportView(tablaPublicoGeneral);

        botonAgregarUsuario.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/añadir con color negro.png"))); // NOI18N
        botonAgregarUsuario.setText("Registrar");
        botonAgregarUsuario.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonAgregarUsuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarUsuarioActionPerformed(evt);
            }
        });

        botonEditarPublico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/eliminar en color negro.png"))); // NOI18N
        botonEditarPublico.setText("Editar");
        botonEditarPublico.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonEditarPublico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarPublicoActionPerformed(evt);
            }
        });

        botonBajaPublico.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/eliminar en color negro (2).png"))); // NOI18N
        botonBajaPublico.setText("Baja");
        botonBajaPublico.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonBajaPublico.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBajaPublicoActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/icons8-editar-propiedad-24.png"))); // NOI18N
        jButton4.setText("Registrar consulta");
        jButton4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/icons8-ms-excel-24.png"))); // NOI18N
        jButton1.setText("Generar Excel");
        jButton1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        verTodos.setText("Ver todos");
        verTodos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                verTodosActionPerformed(evt);
            }
        });

        entradaDeBusqueda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                entradaDeBusquedaActionPerformed(evt);
            }
        });

        botonBuscar.setText("Buscar");
        botonBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        jLabel3.setText("Publico general");

        javax.swing.GroupLayout panelLateral2Layout = new javax.swing.GroupLayout(panelLateral2);
        panelLateral2.setLayout(panelLateral2Layout);
        panelLateral2Layout.setHorizontalGroup(
            panelLateral2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateral2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addGap(120, 120, 120)
                .addComponent(verTodos)
                .addGap(18, 18, 18)
                .addComponent(entradaDeBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 309, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscar)
                .addContainerGap(487, Short.MAX_VALUE))
        );
        panelLateral2Layout.setVerticalGroup(
            panelLateral2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateral2Layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(panelLateral2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(verTodos)
                    .addComponent(entradaDeBusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonBuscar)
                    .addComponent(jLabel3))
                .addContainerGap(22, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelPublicoGeneralLayout = new javax.swing.GroupLayout(panelPublicoGeneral);
        panelPublicoGeneral.setLayout(panelPublicoGeneralLayout);
        panelPublicoGeneralLayout.setHorizontalGroup(
            panelPublicoGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPublicoGeneralLayout.createSequentialGroup()
                .addGroup(panelPublicoGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPublicoGeneralLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panelPublicoGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(botonEditarPublico, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonBajaPublico, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton4, javax.swing.GroupLayout.DEFAULT_SIZE, 172, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(botonAgregarUsuario, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(90, 90, 90)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 953, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelLateral2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(728, 728, 728))
        );
        panelPublicoGeneralLayout.setVerticalGroup(
            panelPublicoGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPublicoGeneralLayout.createSequentialGroup()
                .addComponent(panelLateral2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelPublicoGeneralLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelPublicoGeneralLayout.createSequentialGroup()
                        .addComponent(botonAgregarUsuario)
                        .addGap(18, 18, 18)
                        .addComponent(botonEditarPublico, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(botonBajaPublico, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton4)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 466, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(58, Short.MAX_VALUE))
        );

        Estadisticas.addTab("Gestion de Publico general", panelPublicoGeneral);

        tablaConsulta.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null}
            },
            new String [] {
                "Numero", "Fecha", "Apellido", "Nombre ", "Dni", "Tipo", "Descripcion"
            }
        ));
        jScrollPane3.setViewportView(tablaConsulta);

        botonEditarConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/eliminar en color negro.png"))); // NOI18N
        botonEditarConsulta.setText("Editar");
        botonEditarConsulta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        botonEditarConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarConsultaActionPerformed(evt);
            }
        });

        bajaConsulta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/eliminar en color negro (2).png"))); // NOI18N
        bajaConsulta.setText("Baja");
        bajaConsulta.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        bajaConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bajaConsultaActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/resources/icons8-ms-excel-24.png"))); // NOI18N
        jButton6.setText("Generar Excel");
        jButton6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        panelLateral3.setPreferredSize(new java.awt.Dimension(1200, 100));

        botonCargarConsultas.setText("ver todos");
        botonCargarConsultas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCargarConsultasActionPerformed(evt);
            }
        });

        botonBuscarConsulta.setText("Buscar");
        botonBuscarConsulta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBuscarConsultaActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("DejaVu Sans", 0, 18)); // NOI18N
        jLabel4.setText("Consultas");

        javax.swing.GroupLayout panelLateral3Layout = new javax.swing.GroupLayout(panelLateral3);
        panelLateral3.setLayout(panelLateral3Layout);
        panelLateral3Layout.setHorizontalGroup(
            panelLateral3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateral3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addGap(180, 180, 180)
                .addComponent(botonCargarConsultas)
                .addGap(18, 18, 18)
                .addComponent(entradaBuscarConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 330, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(botonBuscarConsulta)
                .addContainerGap(464, Short.MAX_VALUE))
        );
        panelLateral3Layout.setVerticalGroup(
            panelLateral3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelLateral3Layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addGroup(panelLateral3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonCargarConsultas)
                    .addComponent(botonBuscarConsulta)
                    .addComponent(jLabel4)
                    .addComponent(entradaBuscarConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(24, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout panelConsultasLayout = new javax.swing.GroupLayout(panelConsultas);
        panelConsultas.setLayout(panelConsultasLayout);
        panelConsultasLayout.setHorizontalGroup(
            panelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultasLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonEditarConsulta, javax.swing.GroupLayout.PREFERRED_SIZE, 171, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(bajaConsulta, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton6, javax.swing.GroupLayout.DEFAULT_SIZE, 171, Short.MAX_VALUE)))
                .addGap(90, 90, 90)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 954, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(119, 119, 119))
            .addGroup(panelConsultasLayout.createSequentialGroup()
                .addComponent(panelLateral3, javax.swing.GroupLayout.PREFERRED_SIZE, 1258, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        panelConsultasLayout.setVerticalGroup(
            panelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelConsultasLayout.createSequentialGroup()
                .addComponent(panelLateral3, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(panelConsultasLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelConsultasLayout.createSequentialGroup()
                        .addComponent(botonEditarConsulta)
                        .addGap(18, 18, 18)
                        .addComponent(bajaConsulta)
                        .addGap(18, 18, 18)
                        .addComponent(jButton6))
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(97, Short.MAX_VALUE))
        );

        Estadisticas.addTab("Gestion de Consultas", panelConsultas);

        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Equipo PEM noviembre 2019");

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1))
            .addComponent(Estadisticas, javax.swing.GroupLayout.PREFERRED_SIZE, 1255, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(Estadisticas, javax.swing.GroupLayout.PREFERRED_SIZE, 641, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void entradaDeBusquedaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_entradaDeBusquedaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_entradaDeBusquedaActionPerformed

    private void botonAgregarUsuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarUsuarioActionPerformed
        cargaPublico.setVisible(true);
    }//GEN-LAST:event_botonAgregarUsuarioActionPerformed

    private void botonBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarActionPerformed

        //DefaultTableModel t = new DefaultTableModel(data, columnNames)
        int dni = Integer.parseInt(this.entradaDeBusqueda.getText());

        List<PublicoGeneral> publicoLista = repositorio.consultaPublicoGeneral(dni);

        cargarTablaPublicoGeneral(publicoLista);


    }//GEN-LAST:event_botonBuscarActionPerformed

    private void verTodosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_verTodosActionPerformed
        this.cargarTablaPublico();
    }//GEN-LAST:event_verTodosActionPerformed

    private void EstadisticasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_EstadisticasMouseClicked
        //System.out.println("Haciendo click en consultas");
    }//GEN-LAST:event_EstadisticasMouseClicked
    //funcion para agregar emprededor
    private void botonRegistrarEmprendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonRegistrarEmprendedorActionPerformed
        cargarEmprendedor.setVisible(true);
    }//GEN-LAST:event_botonRegistrarEmprendedorActionPerformed

    private void botonBuscarEmprendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarEmprendedorActionPerformed
        int dni = Integer.parseInt(entradaBuscarEmprendedor.getText());

        List<Emprededor> lista = repositorio.getEmprendedores(dni);

        cargarTablaEmprendedor(lista);
    }//GEN-LAST:event_botonBuscarEmprendedorActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        cargarTablaEmprededor();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void botonBuscarConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBuscarConsultaActionPerformed
        int dni = Integer.parseInt(entradaBuscarConsulta.getText());

        List<Consulta> lista = repositorio.getConsultas(dni);

        cargarTablaConsulta(lista);
    }//GEN-LAST:event_botonBuscarConsultaActionPerformed

    private void botonCargarConsultasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCargarConsultasActionPerformed
        cargarTablaConsulta();
    }//GEN-LAST:event_botonCargarConsultasActionPerformed

    private void botonEditarEmprendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarEmprendedorActionPerformed
        if (tablaEmprededor.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un emprendedor");
        } else {

            editarEmprendedor.cargaModal();
        }
    }//GEN-LAST:event_botonEditarEmprendedorActionPerformed

    private void botonEditarPublicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarPublicoActionPerformed
        if (tablaPublicoGeneral.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un ciudadano");
        } else {
            editarPublico.cargaModal();
        }


    }//GEN-LAST:event_botonEditarPublicoActionPerformed

    private void botonEditarConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarConsultaActionPerformed
        if (tablaConsulta.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar una consulta");
        } else {
            editarConsulta.cargaModal();
        }
    }//GEN-LAST:event_botonEditarConsultaActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        if (tablaPublicoGeneral.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un ciudadano");
        } else {

            cargaConsulta.setTablaPublico(this.tablaPublicoGeneral);
            cargaConsulta.setVisible(true);
        }
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        if (tablaEmprededor.getSelectedRow() == -1) {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un emprendedor");
        } else {
            cargaConsulta.setTablaPublico(this.tablaEmprededor);
            cargaConsulta.setVisible(true);
        }


    }//GEN-LAST:event_jButton8ActionPerformed

    private void botonBajaPublicoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBajaPublicoActionPerformed
        if (tablaPublicoGeneral.getSelectedRow() != -1) {

            int fila = tablaPublicoGeneral.getSelectedRow();
            int columna = 0;
            int id_publico = Integer.parseInt(tablaPublicoGeneral.getValueAt(fila, columna).toString());
            String nombre = tablaPublicoGeneral.getValueAt(fila, ++columna).toString();
            String apellido = tablaPublicoGeneral.getValueAt(fila, ++columna).toString();

            int confirmado = JOptionPane.showConfirmDialog(
                    this,
                    "¿Estas seguro de borrar a " + nombre + " " + apellido + "?");

            if (JOptionPane.OK_OPTION == confirmado) {
                System.out.println("confirmado");
                System.out.println(id_publico);
                repositorio.bajaPublicoGeneral(id_publico);

                cargarTablaPublico();
                cargarTablaEmprededor();

            } else {
                System.out.println("vale... no borro nada...");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un ciudadano");
        }
    }//GEN-LAST:event_botonBajaPublicoActionPerformed

    private void bajaConsultaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bajaConsultaActionPerformed
        if (tablaConsulta.getSelectedRow() != -1) {

            int fila = tablaConsulta.getSelectedRow();
            int columnId = 0, columna = 4;
            int id_consulta = Integer.parseInt(tablaConsulta.getValueAt(fila, columnId).toString());

            String nombre = tablaConsulta.getValueAt(fila, ++columna).toString();

            String apellido = tablaConsulta.getValueAt(fila, ++columna).toString();

            int confirmado = JOptionPane.showConfirmDialog(
                    this,
                    "¿Estas seguro de borrar a " + nombre + " " + apellido + "?");

            if (JOptionPane.OK_OPTION == confirmado) {
                System.out.println("confirmado");
                System.out.println(id_consulta);
                repositorio.bajaConsulta(id_consulta);

                cargarTablaConsulta();
            } else {
                System.out.println("vale... no borro nada...");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un ciudadano");
        }
    }//GEN-LAST:event_bajaConsultaActionPerformed
    private void botonBajaEmprendedorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBajaEmprendedorActionPerformed
        if (tablaEmprededor.getSelectedRow() != -1) {

            int fila = tablaEmprededor.getSelectedRow();
            int columna = 0;
            int id_emprendedor = Integer.parseInt(tablaEmprededor.getValueAt(fila, columna).toString());

            String nombre = tablaEmprededor.getValueAt(fila, ++columna).toString();
            String apellido = tablaEmprededor.getValueAt(fila, ++columna).toString();

            int confirmado = JOptionPane.showConfirmDialog(
                    this,
                    "¿Estas seguro de borrar a " + nombre + " " + apellido + "?");

            if (JOptionPane.OK_OPTION == confirmado) {
                System.out.println("confirmado");
                System.out.println(id_emprendedor);
                repositorio.bajaEmprendedor(id_emprendedor);
                cargarTablaEmprededor();

            } else {
                System.out.println("vale... no borro nada...");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Debe seleccionar un emprendedor");

        }
    }//GEN-LAST:event_botonBajaEmprendedorActionPerformed

    public File FileGuardarArchivo() {

        JFileChooser guardar = new JFileChooser();
        guardar.showSaveDialog(null);
        guardar.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        File archivo = guardar.getSelectedFile();

        return archivo;
    }

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        try {

            File destino = FileGuardarArchivo();

            if (destino != null) {
                exportarDatos.generarExcel(tablaPublicoGeneral, new File(destino.getAbsolutePath() + ".xls"));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        try {

            File destino = FileGuardarArchivo();

            if (destino != null) {
                exportarDatos.generarExcel(tablaEmprededor, new File(destino.getAbsolutePath() + ".xls"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        try {
            File destino = FileGuardarArchivo();

            if (destino != null) {
                exportarDatos.generarExcel(tablaConsulta, new File(destino.getAbsolutePath() + ".xls"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }//GEN-LAST:event_jButton6ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FramePrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane Estadisticas;
    private javax.swing.JPanel PanelGestionEmprendedores;
    private javax.swing.JButton bajaConsulta;
    private javax.swing.JButton botonAgregarUsuario;
    private javax.swing.JButton botonBajaEmprendedor;
    private javax.swing.JButton botonBajaPublico;
    private javax.swing.JButton botonBuscar;
    private javax.swing.JButton botonBuscarConsulta;
    private javax.swing.JButton botonBuscarEmprendedor;
    private javax.swing.JButton botonCargarConsultas;
    private javax.swing.JButton botonEditarConsulta;
    private javax.swing.JButton botonEditarEmprendedor;
    private javax.swing.JButton botonEditarPublico;
    private javax.swing.JButton botonRegistrarEmprendedor;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.JTextField entradaBuscarConsulta;
    private javax.swing.JTextField entradaBuscarEmprendedor;
    private javax.swing.JTextField entradaDeBusqueda;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JPanel panelConsultas;
    private javax.swing.JPanel panelLateral1;
    private javax.swing.JPanel panelLateral2;
    private javax.swing.JPanel panelLateral3;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JPanel panelPublicoGeneral;
    private javax.swing.JTable tablaConsulta;
    private javax.swing.JTable tablaEmprededor;
    private javax.swing.JTable tablaPublicoGeneral;
    private javax.swing.JButton verTodos;
    // End of variables declaration//GEN-END:variables
}
