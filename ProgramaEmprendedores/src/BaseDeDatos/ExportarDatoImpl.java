/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDeDatos;

import java.io.File;
import javax.swing.JTable;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 *
 * @author david
 */
public class ExportarDatoImpl implements ExportarDatos {

    @Override

    public void generarExcel(JTable tabla, File destino) {
        /**
         * crea un tabla del excel
         *
         */

        
        try {

            WorkbookSettings configuraciones = new WorkbookSettings();
            configuraciones.setEncoding("ISO-8859-1");

            WritableWorkbook tablaExcel = Workbook.createWorkbook(destino, configuraciones);

            WritableSheet hoja = tablaExcel.createSheet("Resultados", 0);

            int cantidadColumnas = tabla.getColumnCount();

            int i = 0;
            for (; i < cantidadColumnas; i++) {
                hoja.addCell(new jxl.write.Label(i, 0, tabla.getColumnName(i)));
            }

            for (int fila = 1; fila <= tabla.getRowCount(); fila++) {
                for (int columna = 0; columna < cantidadColumnas; columna++) {
                    if (tabla.getValueAt(fila - 1, columna) != null) {
                        hoja.addCell(new jxl.write.Label(columna, fila, tabla.getValueAt(fila - 1, columna).toString()));
                    }
                }
            }

            tablaExcel.write();
            tablaExcel.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
