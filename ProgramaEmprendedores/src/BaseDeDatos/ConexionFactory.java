/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDeDatos;

/**
 *
 * @author Empleo
 */
import java.sql.*;
public class ConexionFactory {
    
   private static final String usuario = "root"; 
   private static final String password = "CursoSql1234";
  
   
   private static Connection conexion = null;
   
   public static Connection obtener() throws SQLException, ClassNotFoundException {
      if (conexion == null) {
        
          try {
              
            Class.forName("com.mysql.jdbc.Driver");
            conexion = DriverManager.getConnection("jdbc:mysql://localhost/idel", usuario, password);
         
          } catch (SQLException ex) {
             System.out.println("No se puede conectar con la base de datos");
            throw new SQLException(ex);
         } catch (ClassNotFoundException ex) {
             System.out.println("Error con las librerias");
            throw new ClassCastException(ex.getMessage());
         }
      }
      return conexion;
   }
   
   public static void cerrar() throws SQLException {
      if (conexion != null) {
         conexion.close();
      }
   }
}
