/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;


import java.time.LocalDate;

/**
 *
 * @author Empleo
 */
public class Empleado extends Persona {

    private String userName;
    private String password;
    private String cargo;
    private String correo;
    private int telefono;
   

   
    

    public Empleado(String userName, String password, String nombre, String apellido, int dni, LocalDate fechaNacimiento) {
        super(nombre, apellido, dni, null,fechaNacimiento);
        this.userName = userName;
        this.password = password;
    }

    public Empleado(String nombre, String apellido, int dni, String cargo, String correo, int telefono){
        super(nombre, apellido, dni, null,null);
        this.cargo = cargo;
        this.correo = correo;
        this.telefono = telefono;
    }
    
    

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }
    
     public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

   
    
    public String toString(){
        return "nombre:" + this.getNombre() + " apellido:" + this.getApellido() + " DNI:" + this.getDni();
    }

}
