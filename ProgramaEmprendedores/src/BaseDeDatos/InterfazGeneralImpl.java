/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDeDatos;

import Entidades.Barrio;
import Entidades.Consulta;
import Entidades.Empleado;
import Entidades.Emprededor;
import Entidades.Localidad;
import Entidades.Persona;
import Entidades.PublicoGeneral;
import Entidades.Rubro;
import Entidades.tipoConsulta;
import java.io.File;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author David
 */
public class InterfazGeneralImpl implements InterfazGeneral {

    private Connection persistencia;

    public InterfazGeneralImpl() {
        try {
            persistencia = ConexionFactory.obtener();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<Empleado> getAdministrador() {
        List<Empleado> empleados = new ArrayList<>();

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT p.nombre, p.apellido, p.dni, e.cargo, e. correo, e.telefono  \n"
                    + "FROM empleado AS e INNER JOIN persona AS p\n"
                    + " ON(e.persona = p.id)");

            ResultSet rsult = query.executeQuery();

            while (rsult.next()) {
                empleados.add(new Empleado(rsult.getString(1), rsult.getString(2), rsult.getInt(3), rsult.getString(4), rsult.getString(5), rsult.getInt(6)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return empleados;
    }

    @Override
    public void setAdministrador(Empleado administrador) {

    }

    @Override
    public List<PublicoGeneral> getPublicoGeneral() {
        /**
         * devuelve una lista registrado en el sistema
         */
        List<PublicoGeneral> publicoLista = new ArrayList<>();
        PublicoGeneral publico;

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT p.nombre, p.apellido, p.dni, m.nombre, b.nombre, s.nombre, pu.telefono, pu.celular, p.fechaDeNac,pu.fechaDeReg, pu.id as 'id'\n"
                    + "FROM publico AS pu \n"
                    + "INNER JOIN persona AS p  ON(pu.persona = p.id)\n"
                    + "INNER JOIN sexo AS s ON (p.sexo = s.id)\n"
                    + "INNER JOIN barrio AS b ON(pu.barrio = b.id)\n"
                    + "INNER JOIN municipio AS m ON(b.municipio = m.id)\n"
                    + "WHERE pu.estado = 1");

            ResultSet resultado = query.executeQuery();
            //Base de datos :#1-nombre, 2-apellido, 3-dni, 4-localidad, 5-barrio, 6-sexo, 7-telefono, 8-celular,9-p.fechaNacimiento, 10-fechaRegis

            //Constructor: nombre, apellido, dni, String sexo, LocalDate fechaNacimiento, String localidad, String barrio, int telefono, int celular
            while (resultado.next()) {
                publico = new PublicoGeneral(resultado.getString(1), resultado.getString(2), resultado.getInt(3), resultado.getString(6), resultado.getDate(9).toLocalDate());

                publico.setCelular(resultado.getInt(8));
                publico.setTelefono(resultado.getInt(7));
                publico.setLocalidad(resultado.getString(4));
                publico.setBarrio(resultado.getString(5));
                publico.setCodigoPublico(resultado.getInt("id"));

                if (resultado.getDate(10) != null) {
                    publico.setFechaRegistro(resultado.getDate(10).toLocalDate());
                }

                publicoLista.add(publico);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return publicoLista;
    }

    @Override
    public void setPublicoGeneral(PublicoGeneral publico) {
        setPersona(new Persona(publico.getNombre(), publico.getApellido(), publico.getDni(), publico.getSexo(), publico.getFechaNacimiento()));

        String busquda = "SELECT p.id FROM persona as p WHERE p.dni=" + publico.getDni();

        String instrucciones = "INSERT INTO publico(persona, barrio, telefono, celular, fechaDeReg) VALUE (?,?,?,?,current_date())";

        try {
            PreparedStatement queryBusqueda = persistencia.prepareStatement(busquda);
            PreparedStatement query = persistencia.prepareStatement(instrucciones);

            ResultSet resultado = queryBusqueda.executeQuery();

            int limitador = 1;
            while (resultado.next() & limitador == 1) {
                query.setInt(1, resultado.getInt(1));
                query.setInt(2, Integer.parseInt(publico.getBarrio()));
                query.setInt(3, publico.getTelefono());
                query.setInt(4, publico.getCelular());
                query.executeUpdate();

                ++limitador;
            }

        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    @Override
    public void setPersona(Persona persona) {

        String instruciones = "INSERT INTO persona(nombre, apellido, dni, sexo,fechaDeNac) VALUE (?, ?, ?, ?, ?)";

        try {
            PreparedStatement query = persistencia.prepareStatement(instruciones);

            query.setString(1, persona.getNombre());
            query.setString(2, persona.getApellido());
            query.setInt(3, persona.getDni());
            if (persona.getSexo().equals("masculino")) {
                query.setInt(4, 1);
            } else {
                query.setInt(4, 2);
            }
            query.setObject(5, persona.getFechaNacimiento().toString());
            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Persona> getPersonas() {
        List<Persona> personas = new ArrayList<>();

        Persona persona;
        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT p.id, p.nombre, p.apellido, p.dni, s.nombre, p.fechaDeNac \n"
                    + "FROM persona as p\n"
                    + "INNER JOIN sexo as s\n"
                    + "ON(p.sexo = s.id)");
            ResultSet resultado = query.executeQuery();

            while (resultado.next()) {
                //1-id, 2-nombre 3-apellido 4-dni 5-sexo 6-fecha de Naci

                persona = new Persona(resultado.getString(2), resultado.getString(3), resultado.getInt(4), resultado.getString(5), resultado.getDate(6).toLocalDate());
                personas.add(persona);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return personas;
    }

    @Override
    public List<Localidad> getMunicipios() {
        /**
         * retorna los domicilos
         */
        List<Localidad> municipios = new ArrayList<>();

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT d.nombre FROM municipio as d");
            ResultSet resultado = query.executeQuery();

            while (resultado.next()) {
                municipios.add(new Localidad(resultado.getString(1)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return municipios;
    }

    @Override
    public void setMunicipio(Localidad localidad) {

    }

    @Override
    public List<Barrio> getBarrios(Localidad localidad) {
        List<Barrio> barrios = new ArrayList<>();

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT b.nombre FROM barrio AS b\n"
                    + "WHERE b.municipio =" + localidad.getId());

            ResultSet resultado = query.executeQuery();

            while (resultado.next()) {
                barrios.add(new Barrio(resultado.getString(1)));
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return barrios;
    }

    @Override
    public void setBarrio(Barrio barrio, Localidad localidad) {

    }

    @Override
    public void setConsulta(Consulta consulta, PublicoGeneral ciudadano) {
        String scrip = "insert into consulta(descripcion, tipo, publicoGeneral,fecha) value (?,?,?,current_date())";
        String scripTipoConsulta = "select t.id from tipoConsulta as t where t.nombre = '" + consulta.getTipo() + "'";

        try {

            PreparedStatement queryTipo = persistencia.prepareStatement(scripTipoConsulta);
            PreparedStatement query = persistencia.prepareStatement(scrip);

            ResultSet resultado = queryTipo.executeQuery();

            while (resultado.next()) {
                int idTipoConsulta = resultado.getInt("id");

                query.setString(1, consulta.getDescripcion());
                query.setInt(2, idTipoConsulta);
                query.setInt(3, ciudadano.getCodigoPublico());

            }

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<Consulta> getConsultas() {
        List<Consulta> consultas = new ArrayList<>();
        Consulta consulta = null;
        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT c.id, c.descripcion, t.nombre as 'tipo', p.nombre, p.apellido, p.dni, c.fecha\n"
                    + "FROM publico AS pu \n"
                    + "INNER JOIN persona AS p  ON(pu.persona = p.id)\n"
                    + "inner join consulta as c on(c.publicoGeneral = pu.id)\n"
                    + "inner join tipoConsulta as t on(c.tipo = t.id) where c.estado=1");

            ResultSet resultado = query.executeQuery();
            //1 id 2 nombre 3 descripcion
            while (resultado.next()) {

                int numero = resultado.getInt("id");
                String descripcion = resultado.getString("descripcion");
                String tipo = resultado.getString("tipo");
                String nombre = resultado.getString("nombre");
                String apellido = resultado.getString("apellido");
                int dni = resultado.getInt("dni");
                LocalDate fecha = resultado.getDate("fecha").toLocalDate();

                consulta = new Consulta(numero, dni, nombre, apellido, tipo, descripcion, fecha);
                consultas.add(consulta);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return consultas;
    }

    @Override
    public List<Emprededor> getEmprededores() {
        /**
         * devuelve una lista de empredores
         */
        List<Emprededor> emprededores = new ArrayList<>();
        Emprededor emprededor = null;

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT pe.fechaDeNac,e.id, pe.nombre, pe.apellido, pe.dni, m.nombre AS 'municipio', b.nombre AS 'barrio', s.nombre AS 'sexo', p.celular,p.telefono, e.correo, r.nombre AS 'rubro' FROM emprendedor AS e\n"
                    + " INNER JOIN publico AS p ON(p.id = e.publico) \n"
                    + " INNER JOIN persona AS pe ON(pe.id = p.persona) \n"
                    + " INNER JOIN rubro AS r ON(r.id = e.rubro) \n"
                    + " INNER JOIN sexo AS s ON(s.id = pe.sexo) \n"
                    + " INNER JOIN barrio AS b ON(b.id = p.barrio) \n"
                    + " INNER JOIN municipio AS m ON(b.municipio = m.id) \n"
                    + "WHERE p.estado = 1 AND e.estado = 1"
            );
            ResultSet resultado = query.executeQuery();

            //1 nombre 2 apellido 3 dni
            while (resultado.next()) {
                int id_emprendedor = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String apellido = resultado.getString("apellido");
                int dni = resultado.getInt("dni");
                String municipio = resultado.getString("municipio");
                String barrio = resultado.getString("barrio");
                String sexo = resultado.getString("sexo");
                int celular = resultado.getInt("celular");
                int telefono = resultado.getInt("telefono");
                String correo = resultado.getString("correo");
                String rubro = resultado.getString("rubro");
                emprededor = new Emprededor(nombre, apellido, dni, sexo, null, municipio, barrio, telefono, celular);
                emprededor.setCorreo(correo);
                emprededor.setRubro(rubro);
                emprededores.add(emprededor);
                emprededor.setIdEmprendedor(id_emprendedor);
                emprededor.setFechaNacimiento(resultado.getDate("fechaDeNac").toLocalDate());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emprededores;

    }

    @Override
    public void setEmprededor(Emprededor e) {
        setPublicoGeneral(e);
        
        String busqueda = "SELECT pu.id\n"
                + "FROM publico AS pu \n"
                + "INNER JOIN persona AS p  ON(pu.persona = p.id)\n"
                + "WHERE p.dni =" + e.getDni();

        String instrucciones = "INSERT INTO emprendedor(correo, rubro, publico, fechaRegistro) VALUE (?,?, ?,CURRENT_DATE())";

        try {
            PreparedStatement queryBusqueda = persistencia.prepareStatement(busqueda);
            PreparedStatement query = persistencia.prepareStatement(instrucciones);

            ResultSet resultado = queryBusqueda.executeQuery();

            int limitador = 1;
            while (resultado.next() & limitador == 1) {
                query.setString(1, e.getCorreo());
                query.setInt(2, Integer.parseInt(e.getRubro()));
                query.setInt(3, resultado.getInt(1));

                query.executeUpdate();

                System.out.println("Inscipcion exitosa!");

                ++limitador;
            }

        } catch (SQLException err) {
            err.printStackTrace();
        }
    }

    @Override
    public List<tipoConsulta> gettipoConsultas() {

        List<tipoConsulta> tipos = new ArrayList<>();
        tipoConsulta consulta = null;

        try {
            PreparedStatement query = persistencia.prepareStatement("select*from tipoConsulta");

            ResultSet resultado = query.executeQuery();
            //1 id 2 nombre 3 descripcion
            while (resultado.next()) {
                int id = resultado.getInt("id");
                String nombre = resultado.getString("nombre");
                String descripcion = resultado.getString("descripcion");

                consulta = new tipoConsulta(id, descripcion, nombre);

                tipos.add(consulta);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return tipos;
    }

    @Override
    public List<PublicoGeneral> consultaPublicoGeneral(int dni) {
        List<PublicoGeneral> publicoLista = new ArrayList<>();
        PublicoGeneral publico;

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT p.nombre, p.apellido, p.dni, m.nombre, b.nombre, s.nombre, pu.telefono, pu.celular, p.fechaDeNac,pu.fechaDeReg, pu.id as 'idPublico', p.id as 'idPersona'\n"
                    + "FROM publico AS pu \n"
                    + "INNER JOIN persona AS p  ON(pu.persona = p.id)\n"
                    + "INNER JOIN sexo AS s ON (p.sexo = s.id)\n"
                    + "INNER JOIN barrio AS b ON(pu.barrio = b.id)\n"
                    + "INNER JOIN municipio AS m ON(b.municipio = m.id)\n"
                    + "WHERE pu.estado = 1 AND p.dni =" + dni);

            ResultSet resultado = query.executeQuery();
            //Base de datos :#1-nombre, 2-apellido, 3-dni, 4-localidad, 5-barrio, 6-sexo, 7-telefono, 8-celular,9-p.fechaNacimiento, 10-fechaRegis

            //Constructor: nombre, apellido, dni, String sexo, LocalDate fechaNacimiento, String localidad, String barrio, int telefono, int celular
            while (resultado.next()) {
                publico = new PublicoGeneral(resultado.getString(1), resultado.getString(2), resultado.getInt(3), resultado.getString(6), resultado.getDate(9).toLocalDate());

                publico.setCelular(resultado.getInt(8));
                publico.setTelefono(resultado.getInt(7));
                publico.setLocalidad(resultado.getString(4));
                publico.setBarrio(resultado.getString(5));

                if (resultado.getDate(10) != null) {
                    publico.setFechaRegistro(resultado.getDate(10).toLocalDate());
                }

                publico.setId(resultado.getInt("idPersona"));
                publico.setCodigoPublico(resultado.getInt("idPublico"));

                publicoLista.add(publico);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return publicoLista;
    }

    @Override
    public List<PublicoGeneral> getPublicoUltimoAnotados() {
        List<PublicoGeneral> ultimosAnotados = new ArrayList<>();

        PublicoGeneral publico = null;

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT p.nombre, p.apellido, p.dni, m.nombre as 'municipio', b.nombre as 'barrio', s.nombre as 'sexo', pu.telefono, pu.celular, p.fechaDeNac,pu.fechaDeReg, pu.id as 'codigo'\n"
                    + "FROM publico AS pu \n"
                    + "INNER JOIN persona AS p  ON(pu.persona = p.id)\n"
                    + "INNER JOIN sexo AS s ON (p.sexo = s.id)\n"
                    + "INNER JOIN barrio AS b ON(pu.barrio = b.id)\n"
                    + "INNER JOIN municipio AS m ON(b.municipio = m.id)\n"
                    + "order by p.id desc\n"
                    + "limit 4");

            ResultSet resultado = query.executeQuery();

            while (resultado.next()) {
                publico = new PublicoGeneral(resultado.getString(1), resultado.getString(2), resultado.getInt(3), resultado.getString(6), resultado.getDate(9).toLocalDate());

                publico = new PublicoGeneral(resultado.getString("nombre"), resultado.getString("apellido"), resultado.getInt("dni"), null, null);
                /*publico.setCelular(resultado.getInt(8));
                publico.setTelefono(resultado.getInt(7));
                publico.setLocalidad(resultado.getString(4));
                publico.setBarrio(resultado.getString(5));*/

                publico.setCodigoPublico(resultado.getInt("codigo"));

                ultimosAnotados.add(publico);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return ultimosAnotados;
    }

    @Override
    public List<Rubro> getRubros() {
        List<Rubro> rubros = new ArrayList<>();

        Rubro rubro = null;
        try {
            PreparedStatement query = persistencia.prepareStatement("select * from rubro");

            ResultSet resultado = query.executeQuery();

            while (resultado.next()) {
                rubro = new Rubro(resultado.getString("nombre"));
                rubro.setId(resultado.getInt("id"));

                rubros.add(rubro);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return rubros;
    }

    @Override
    public List<Emprededor> getEmprendedores(int dni) {
        List<Emprededor> emprededores = new ArrayList<>();

        Emprededor emprededor = null;

        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT pe.id as 'idPersona', p.id as 'idPublico', e.id as 'idEmprendedor',pe.fechaDeNac, pe.nombre, pe.apellido, pe.dni, m.nombre AS 'municipio', b.nombre AS 'barrio', s.nombre AS 'sexo', p.celular,p.telefono, e.correo, r.nombre AS 'rubro' FROM emprendedor AS e\n"
                    + " INNER JOIN publico AS p ON(p.id = e.publico) \n"
                    + " INNER JOIN persona AS pe ON(pe.id = p.persona) \n"
                    + " INNER JOIN rubro AS r ON(r.id = e.rubro) \n"
                    + " INNER JOIN sexo AS s ON(s.id = pe.sexo) \n"
                    + " INNER JOIN barrio AS b ON(b.id = p.barrio) \n"
                    + " INNER JOIN municipio AS m ON(b.municipio = m.id) WHERE e.estado = 1 AND pe.dni=" + dni);
            ResultSet resultado = query.executeQuery();

            //1 nombre 2 apellido 3 dni
            while (resultado.next()) {
                String nombre = resultado.getString("nombre");
                String apellido = resultado.getString("apellido");
                int dniBase = resultado.getInt("dni");
                String municipio = resultado.getString("municipio");
                String barrio = resultado.getString("barrio");
                String sexo = resultado.getString("sexo");
                int celular = resultado.getInt("celular");
                int telefono = resultado.getInt("telefono");
                String correo = resultado.getString("correo");
                String rubro = resultado.getString("rubro");
                emprededor = new Emprededor(nombre, apellido, dniBase, sexo, null, municipio, barrio, telefono, celular);
                emprededor.setCorreo(correo);
                emprededor.setRubro(rubro);
                emprededores.add(emprededor);
                emprededor.setFechaNacimiento(resultado.getDate("fechaDeNac").toLocalDate());
                emprededor.setId(resultado.getInt("idPersona"));
                emprededor.setCodigoPublico(resultado.getInt("idPublico"));
                emprededor.setIdEmprendedor(resultado.getInt("idEmprendedor"));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return emprededores;
    }

    @Override
    public List<Consulta> getConsultas(int dni) {
        List<Consulta> consultas = new ArrayList<>();

        Consulta consulta = null;
        try {
            PreparedStatement query = persistencia.prepareStatement("SELECT c.id, c.descripcion, t.nombre as 'tipo', p.nombre, p.apellido, p.dni, c.fecha\n"
                    + "FROM publico AS pu \n"
                    + "INNER JOIN persona AS p  ON(pu.persona = p.id)\n"
                    + "inner join consulta as c on(c.publicoGeneral = pu.id)\n"
                    + "inner join tipoConsulta as t on(c.tipo = t.id) WHERE c.estado = 1 AND p.dni=" + dni);

            ResultSet resultado = query.executeQuery();
            //1 id 2 nombre 3 descripcion
            while (resultado.next()) {

                int numero = resultado.getInt("id");
                String descripcion = resultado.getString("descripcion");
                String tipo = resultado.getString("tipo");
                String nombre = resultado.getString("nombre");
                String apellido = resultado.getString("apellido");
                int dniBase = resultado.getInt("dni");
                LocalDate fecha = resultado.getDate("fecha").toLocalDate();

                consulta = new Consulta(numero, dniBase, nombre, apellido, tipo, descripcion, fecha);
                consultas.add(consulta);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return consultas;
    }

    @Override
    public void updatePersona(Persona persona) {

        String scrip = "UPDATE persona SET nombre=?\n"
                + ",apellido =?,dni =?,sexo=?,fechaDeNac=? WHERE id =" + persona.getId();

        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.setString(1, persona.getNombre());
            query.setString(2, persona.getApellido());
            query.setInt(3, persona.getDni());
            if (persona.getSexo().equals("masculino")) {
                query.setObject(4, 1);
            } else {
                query.setObject(4, 2);
            }

            query.setString(5, persona.getFechaNacimiento().toString());

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updatePublicoGeneral(PublicoGeneral publico) {

        updatePersona(publico);

        String scrip = "update publico \n"
                + "set barrio = ?,\n"
                + "telefono = ?,\n"
                + "celular = ?\n"
                + "where id =" + publico.getCodigoPublico();

        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.setInt(1, Integer.parseInt(publico.getBarrio()));
            query.setInt(2, publico.getTelefono());
            query.setInt(3, publico.getCelular());

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void updateEmprendedor(Emprededor emprendedor) {
       
        updatePublicoGeneral(emprendedor);

        String scrip = "update emprendedor set\n"
                + "correo = ?,\n"
                + "rubro = ?\n"
                + "where id =" + emprendedor.getIdEmprendedor();

        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.setString(1, emprendedor.getCorreo());
            query.setInt(2, Integer.parseInt(emprendedor.getRubro()));

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<PublicoGeneral> getPublicoGeneral(int index, int limitRows) {
        List<PublicoGeneral> lista = new ArrayList<>();

        return lista;
    }

    @Override
    public List<Emprededor> getEmprendedores(int index, int limitRows) {
        List<Emprededor> lista = new ArrayList<>();

        return lista;
    }

    @Override
    public void bajaPublicoGeneral(int key) {

        String scrip = "update publico set estado = 0 where id =" + key;

        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void bajaEmprendedor(int key) {
        String scrip = "update emprendedor set estado = 0 where id =" + key;

        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void bajaConsulta(int key) {
        String scrip = "update consulta set estado = 0 where id =" + key;
        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @Override
    public void updateConsulta(Consulta consulta) {
        String scrip = "update consulta set \n"
                + "descripcion = ?,\n"
                + "tipo = ? \n"
                + "where id =" + consulta.getNumero();
        try {
            PreparedStatement query = persistencia.prepareStatement(scrip);

            query.setString(1, consulta.getDescripcion());
            query.setInt(2, Integer.parseInt(consulta.getTipo()));
            query.executeUpdate();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
