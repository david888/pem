/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package BaseDeDatos;


import Entidades.Barrio;
import Entidades.Consulta;
import Entidades.Empleado;
import Entidades.Emprededor;
import Entidades.Localidad;
import Entidades.Persona;
import Entidades.PublicoGeneral;
import Entidades.Rubro;
import Entidades.tipoConsulta;
import java.io.File;
import java.util.List;
import javax.swing.JTable;

/**
 *
 * @author David
 */
public interface InterfazGeneral {
    
    
    public void setPersona(Persona persona);
    public void updatePersona(Persona persona);
    public List<Persona> getPersonas();
    
    public List<Empleado> getAdministrador();
    public void setAdministrador(Empleado administrador);
    
    public List<PublicoGeneral> getPublicoGeneral();
    public List<PublicoGeneral> getPublicoGeneral(int index, int limitRows);
    public void setPublicoGeneral(PublicoGeneral publico);
    public void updatePublicoGeneral(PublicoGeneral publico);
    public List<PublicoGeneral> consultaPublicoGeneral(int dni);
    public List<PublicoGeneral> getPublicoUltimoAnotados();
    public void bajaPublicoGeneral(int key);
    
    
    public void setEmprededor(Emprededor e);
    public List <Emprededor> getEmprededores();
    public List<Emprededor> getEmprendedores(int dni);
    public void updateEmprendedor(Emprededor emprendedor);
    public List<Emprededor> getEmprendedores(int index, int limitRows);
    public void bajaEmprendedor(int key);
    
    
    public void setConsulta(Consulta consulta, PublicoGeneral ciudadano);
    public List<Consulta> getConsultas();
    public List<Consulta> getConsultas(int dni);
    public void bajaConsulta(int key);
    public void updateConsulta(Consulta consulta);
    
    
    public List<tipoConsulta> gettipoConsultas();
    
    public List<Rubro> getRubros();
    
    public List<Localidad> getMunicipios();
    public void setMunicipio(Localidad localidad);
    public List<Barrio> getBarrios(Localidad localidad);
    public void setBarrio(Barrio barrio, Localidad localidad);
     
    
    
}
