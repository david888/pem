/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.time.LocalDate;

/**
 *
 * @author David
 */
public class Consulta {
    private int numero;
    private String nombre;
    private String apellido;
    private String tipo;
    private String descripcion;
    private LocalDate fecha;
    private int dni;

   

    public Consulta(int numero,int dni, String nombre,String apellido, String tipo, String descripcion, LocalDate fecha) {
        this.numero = numero;
        this.nombre = nombre;
        this.apellido=apellido;
        this.tipo = tipo;
        this.descripcion = descripcion;
        this.fecha = fecha;
        this.dni=dni;
    }
     public void setDni(int dni) {
        this.dni = dni;
    }

    public int getDni() {
        return dni;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public String getApellido() {
        return apellido;
    }
  

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }
 public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    
    
    
    
}
