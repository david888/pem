/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Aplicacion;

import java.time.LocalDate;
import javax.swing.JComboBox;

/**
 *
 * @author david
 */
public class ConvertidorDeFechas {

    public static LocalDate comboBoxAFecha(JComboBox comboAño, JComboBox comboMes, JComboBox comboDia) {
        String dia = comboDia.getSelectedItem().toString();

        int año = Integer.parseInt(comboAño.getSelectedItem().toString());

        String mes = "";

        if (comboMes.getSelectedIndex() < 9) {
            mes = "0" + (comboMes.getSelectedIndex() + 1);
        } else {
            mes = String.valueOf(comboMes.getSelectedIndex() + 1);
        }

        return LocalDate.parse(año + "-" + mes + "-" + dia);
    }

    public static void fechaAComboBox(JComboBox comboAño, JComboBox comboMes, JComboBox comboDia, LocalDate fecha) {
        comboDia.setSelectedIndex(fecha.getDayOfMonth() - 1);
        comboMes.setSelectedIndex(fecha.getMonthValue() - 1);
        //

        int añoActual = LocalDate.now().getYear(), indice = añoActual - fecha.getYear();

        System.out.println(indice);
        
        comboAño.setSelectedIndex(indice);
        //System.out.println(comboMes.getSelectedItem());
        System.out.println(fecha.getDayOfMonth() + " " + fecha.getMonthValue() + " " + fecha.getYear());
    }
    
    public static void generarAños(JComboBox comboAño){
        int añoActual = LocalDate.now().getYear(), limite = añoActual;

        for (; añoActual > (limite - 100); añoActual--) {

            comboAño.addItem(String.valueOf(añoActual));
        }
    }

}
