/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Entidades;

import java.time.LocalDate;

/**
 *
 * @author Empleo
 */
public class PublicoGeneral extends Persona{
    private int idPublico;

   
    private String localidad;
    private String barrio;
    private LocalDate fechaRegistro;
    private int telefono;
    private int celular;

    
    
    public PublicoGeneral(String nombre, String apellido, int dni, String sexo, LocalDate fechaNacimiento, String localidad, String barrio, int telefono, int celular) {
        super(nombre, apellido, dni,sexo,fechaNacimiento);
        this.localidad = localidad;
        this.barrio = barrio;
        this.telefono = telefono;
        this.celular = celular;
        
    }
    
    public PublicoGeneral(String nombre, String apellido, int dni, String sexo,LocalDate fechaNacimiento) {
        super(nombre, apellido, dni,sexo,fechaNacimiento);
    }
    
    public PublicoGeneral(){}
    
    
     public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }
    
    public LocalDate getFechaRegistro() {
        return fechaRegistro;
    }

    public void setFechaRegistro(LocalDate fechaRegistro) {
        this.fechaRegistro = fechaRegistro;
    }
    
    public String toString(){
        return super.toString() + " " +  localidad + " "  + barrio + " " + telefono +" "+ celular + " " + fechaRegistro;
    }
    
     public int getCodigoPublico() {
        return idPublico;
    }

    public void setCodigoPublico(int codigoPublico) {
        this.idPublico = codigoPublico;
    }
}
